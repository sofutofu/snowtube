// Main SRT-loading function
var loadSRT = function(url, callback) {

    fetch(url)
        .then(response => response.text())
        .then(subFile => {
            const subtitles = parser.fromSrt(subFile,true);

            for (var i in subtitles) {
                subtitles[i] = {
                    // Edited to add an offset since subs were coming in a little late
                    start : subtitles[i].startTime / 1000 - 0.4,
                    end   : subtitles[i].endTime / 1000 - 0.4,
                    text  : subtitles[i].text
                };
            }

            callback(subtitles);

        })

    }
    
const videoInfoLoc = "video_info.json"

fetch(videoInfoLoc)
    .then(response => response.json())
    .then(data => {
        var vidInfo = data.values; 
        playlist = document.getElementById("playlist");

    // Populate the playlist
    for(var i=1;i<vidInfo.length;i++){

        selectedVid = vidInfo[i];

        // thumbContainer > videoLoad > thumb + thumbtitle

        thumbContainer = document.createElement('div');
        thumbContainer.className = "thumbcontainer";

        videoLoad = document.createElement('a');
        videoLoad.setAttribute("onclick","switchVid("+JSON.stringify(selectedVid.slice(1,4))+");");

        thumb = document.createElement('img');
        thumb.className = "thumbnail";
        thumb.src = "https://img.youtube.com/vi/"+selectedVid[1]+"/mqdefault.jpg";

        title = document.createElement('p');
        title.className = "thumbtitle";
        title.innerHTML = selectedVid[2];

        appendThumb = videoLoad.appendChild(thumb);
        appendTitle = videoLoad.appendChild(title);
        appendLoad = thumbContainer.appendChild(videoLoad);
        appendVideo = playlist.appendChild(thumbContainer);
    }

    // Default load first item
    document.getElementById("video").setAttribute("src","https://www.youtube.com/embed/"+vidInfo[1][1]+"?enablejsapi=1&html5=1&playsinline=1&fs=0");
    // Place the first item's title and description
    document.getElementById("title").innerHTML = vidInfo[1][2];
    document.getElementById("description").innerHTML = vidInfo[1][3];
    loadSRT('srt/'+vidInfo[1][1]+'.srt', function(subtitles) {
        var youtubeExternalSubtitle = new YoutubeExternalSubtitle.Subtitle(document.getElementById('video'), subtitles);
    });
})

// Swaps the video and subtitle tracks when new item is selected
function switchVid(selectedVideo) {
    var vidID = selectedVideo[0];
    // Update the video
    newVid = document.createElement('iframe');
    newVid.id = 'video';
    newVid.src = "https://www.youtube.com/embed/"+vidID+"?enablejsapi=1&html5=1&playsinline=1&fs=0";
    newVid.frameBorder = "0";
    newVid.allowfullscreen = "true";

    videocontainer = document.getElementById("videocontainer");
    videocontainer.replaceChild(newVid, document.getElementById("video"));

    // Update the title and description
    document.getElementById("title").innerHTML = selectedVideo[1];
    document.getElementById("description").innerHTML = selectedVideo[2].replace(/\n/g,"<br>");

    existingSubtitles = document.getElementsByClassName("youtube-external-subtitle")[0];
    if (existingSubtitles != undefined) {
        existingSubtitles.parentNode.removeChild(existingSubtitles);
    }
    

    loadSRT('srt/'+vidID+'.srt', function(subtitles) {
        var youtubeExternalSubtitle = new YoutubeExternalSubtitle.Subtitle(document.getElementById('video'), subtitles);
    });

};