IwaSakuFC's SnowTube ❄️📺
==========================

## **Check it out at [iwasaku.club](https://iwasaku.club)**

A project to allow English-speaking fans to continue watching subbed versions of [Snow Man](https://www.youtube.com/c/J_SNOWMAN) videos after the demise of the [community contributions](https://support.google.com/youtube/thread/61967856?hl=en) feature on YT. Created for @IwasakuFCGirls by @sofutofu.

Development is entirely indebted to the [youtube.external.subtitle](https://github.com/siloor/youtube.external.subtitle) project.

## Want one for yourself/your fandom/your group?

You're welcome to fork this repo - it's a free country! - but if you'd like some help, you can DM me on Twitter (@sofutofu). I'd be happy to work with you to set up one!

This is a hobbyist effort and likely not suitable for any group that makes or takes money. Please hire an actual developer.

## Branch notes

_As of Nov. 22, 2020_

The `vue` branch has been spun off as `snowtube-vue` to make things less annoying. Please consider the `vue` branch here dead.

## Obligatory

We don't own Snow Man, Johnny's, YouTube, etc. Don't sue us, please. The show must go on!